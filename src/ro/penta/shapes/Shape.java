package ro.penta.shapes;

public abstract class Shape {
	public double perimeter, area;
	public double getPerimeter(){
		return perimeter;
	}
	public double getArea(){
		return area;
	}
	public abstract void calculatePerimeter();
	public abstract void calculateArea();
	public void displayResults(){
		System.out.println("Perimeter = "+perimeter+", Area = "+area);
	}
	

}
