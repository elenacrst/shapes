package ro.penta.shapes.round;

import ro.penta.shapes.Shape;

public class Circle extends Shape{

	double radius;
	
	public Circle(double radius){
		this.radius = radius;
	}
	
	@Override
	public void calculatePerimeter() {
		// TODO Auto-generated method stub
		perimeter = Math.PI * 2 * radius;
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub
		area = Math.PI * radius* radius;
	}

}
