package ro.penta.shapes.straight;

import ro.penta.shapes.Shape;

public class Rectangle extends Shape {

	double width, height;
	
	public Rectangle(double width, double height){
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void calculatePerimeter() {
		// TODO Auto-generated method stub
		perimeter = 2*(width+height);
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub
		area = width* height;
	}
	
}
