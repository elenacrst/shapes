package ro.penta.shapes.straight;

import ro.penta.shapes.Shape;

public class Triangle extends Shape {
	double side;
	
	public Triangle(double side){
		this.side=side;
	}

	@Override
	public void calculatePerimeter() {
		// TODO Auto-generated method stub
		perimeter = 3* side;
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub
		area = side*side * Math.sqrt(3)/4;
	}

	

	
}
