package ro.penta.shapes;
import java.util.Scanner;

import ro.penta.shapes.round.Circle;
import ro.penta.shapes.straight.Rectangle;
import ro.penta.shapes.straight.Triangle;

public class Main {

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scanner = new Scanner(System.in);
		String shape;
		System.out.println("Choose shape: circle / rectangle / triangle");
		shape = scanner.nextLine();
		String str;
		switch(shape){
		case "circle":
			System.out.print("Radius = ");
			str = scanner.nextLine();
			double radius =Double.parseDouble(str);
			Circle circle = new Circle(radius);
			circle.calculateArea();
			circle.calculatePerimeter();
			circle.displayResults();
			break;
		case "rectangle":
			System.out.print("Width = ");
			str = scanner.nextLine();
			double width,height;
			width  = Double.parseDouble(str);
			System.out.print("Height = ");
			str = scanner.nextLine();
			height = Double.parseDouble(str);
			Rectangle rec = new Rectangle(width, height);
			rec.calculateArea();
			rec.calculatePerimeter();
			rec.displayResults();
			
			break;
		case "triangle":
			System.out.print("Side = ");
			str = scanner.nextLine();
			double side;
			side=Double.parseDouble(str);
			Triangle triangle = new Triangle(side);
			triangle.calculateArea();
			triangle.calculatePerimeter();
			triangle.displayResults();
			break;
			
		}
		scanner.close();

	}

}
